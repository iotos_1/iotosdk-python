#安装步骤

- 安装“Ice-3.6.5.msi”
  地址：https://iotosystem.feishu.cn/file/boxcnlqZ4OFsJcy7YTeZbhWAnif

- 安装“python-3.9.7-amd64.exe”

- 安装“vs_buildtools（Microsoft Visual C++ 14.0）.exe”	
   地址：https://iotosystem.feishu.cn/file/boxcnLRUSSzQeWEhJjIwEbIK3Hd
   注意：参考图片，选择指定的安装项。

- 执行“一键安装Python依赖库.bat”