# coding=utf-8
"""扬尘设备驱动"""

import threading
import requests
from datetime import datetime
from driver import IOTOSDriverI
import json, time, random
from library.exception import DataNotExistError
import logging
import uuid
import paho.mqtt.client as mqtt

logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s %(name)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class EabimApi(object):
    session = None  # type: requests.Session

    __baseUrl = 'http://api.xiwon588.com/wisdom/v1/cloudApi'
    __tokenAuth = None  # type: str

    def __init__(self, username, password):
        self.session = requests.Session()
        self.session.headers.setdefault('Content-Type', 'application/x-www-form-urlencoded')
        res = self.login(username, password)
        self.__tokenAuth = res['tokenAuth']
        self.session.headers.setdefault('tokenAuth', self.__tokenAuth)
        print('tokenAuth', self.__tokenAuth)

    def login(self, username, password):
        resp = self.session.post(self.__baseUrl + '/login', data=dict(
            username=username, password=password
        ))
        res = resp.json()
        return res['data']

    def dust_receiveData(self):
        resp = self.session.post(self.__baseUrl + '/dust/receiveData', )

    def device_list(self):
        resp = self.session.get(self.__baseUrl + '/device/list', headers=self.session.headers,
                                params=dict(deviceType='D', pageSize=10, pageNo=1))
        print(resp.request.url)
        print(resp.headers)
        res = resp.json()
        return res

    def device_date(self, deviceId, flag='Y'):
        self.session.headers['User-Agent'] = 'aaaaaa'
        resp = self.session.get(self.__baseUrl + '/device/date',
                                params=dict(deviceType='D', deviceId=deviceId, flag=flag))
        # resp = self.session.get(u'http://api.xiwon588.com/wisdom/v1/cloudApi/device/date?deviceType=D&deviceId=XW202100000221&flag=Y')
        res = resp.json()
        return res['data']


class MQTTPush(object):
    __client = mqtt.Client(client_id=uuid.uuid4().__str__(), protocol=mqtt.MQTTv5)

    def __init__(self):
        self.__client.username_pw_set('test', '123456')
        self.__client.connect('139.196.104.76', 1883, 1)  # 600为keepalive的时间间隔
        self.__client.loop_start()

    def publish(self, _uuid, device_oid, id, oid, value):
        data = dict(
            id=id,
            oid=oid,
            value=value,
            ts=time.time() * 1000
        )
        return self.__client.publish('$IOTOS/event/data/changed/%s/%s/%s' % (_uuid, device_oid, oid),
                                     payload=json.dumps(data))


class DustConfig(object):
    deviceId = None  # type: str
    deviceType = None  # type: str
    username = None  # type: str
    password = None  # type: str

    def __init__(self, deviceId, deviceType, username, password, **kwargs):
        self.deviceId = deviceId
        self.deviceType = deviceType
        self.username = username
        self.password = password


# 继承官方驱动类（ZMIotDriverI）
class DustDriver(IOTOSDriverI):
    __EabimApi = None  # type: EabimApi

    __config = None # type: DustConfig

    # 1、通信初始化
    def InitComm(self, attrs):
        self.online(True)
        self.__config = DustConfig(**attrs['config']['param'])
        # self.setValue(value='auto_created', name='CLI_auto_created')
        self.pauseCollect = False
        self.collectingOneCircle = True
        # self.collectingOneCircle = False
        username = self.__config.username
        password = self.__config.password
        self.__EabimApi = EabimApi(username=username, password=password)
        self.__mqtt = MQTTPush()
        threading.Thread(target=self.__fetch_data, name='EabimApi__fetch_data').start()

    def __fetch_data(self):
        while True:
            try:
                res = self.__EabimApi.device_date(deviceId=self.__config.deviceId)  # type: list[dict]
                if len(res) == 0 or res == '' or res is None:
                    logger.warning(res)
                    time.sleep(5)
                    self.__EabimApi = EabimApi(username=self.__config.username, password=self.__config.password)
                    continue
                for data in res:
                    logger.info(data)
                    for name, value in data.items():
                        try:
                            data_oid = self.id(name)
                            data_id = self.data2attrs[data_oid]['id']
                            rp = self.setValue(name=name, value=value)
                            logger.info('setValue, name=%s, value=%s, rs=%s', name, value, rp)
                            mp = self.__mqtt.publish(_uuid=self.zm.uuid, device_oid=self.sysId, id=data_id,
                                                     oid=data_oid, value=value)
                            logger.info('mqtt.publish, name=%s, value=%s, rs=%s', name, value, mp)
                        except Exception as ex:
                            logger.error('setValue, name=%s, value=%s, rs=%s', name, value, ex, exc_info=True)
                time.sleep(60)
            except Exception:
                logger.error("", exc_info=True)

    firstId = None
    pollTime = None

    # 2、采集
    def Collecting(self, dataId):
        return ()

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        logger.error('dataId=%s, value=%s', dataId, value)
        return json.dumps({'code': 0, 'msg': '', 'data': dict(response_time=time.time())})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
