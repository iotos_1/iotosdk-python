# coding=utf-8
"""考勤系统API"""
import json
import time
import requests
import hashlib
from .dto import CkRecord, CkPerson
from log_utils import new_logger, logging

logger = new_logger(name='checking')
logger.setLevel(logging.INFO)


def to_md5(text):
    m = hashlib.md5()
    m.update(text)
    return m.hexdigest().upper()


class CheckingApi(object):
    __host = None  # type: str
    __token = None  # type: str

    def __init__(self, host, username, password):
        self.__host = host
        token = self.login(username=username, password=password)
        self.__token = token

    def login(self, username, password):
        """
        {u'params': {u'Token': u'50632192433409404740130094300545', u'UserGroup': u'Administrator'}, u'method': u'login', u'result': 0}
        :param username:
        :param password:
        :return:
        """
        url = 'http://{{ip}}/api/system/login'.replace('{{ip}}', self.__host)
        resp = requests.post(url=url, json={
            "params": {
                "UserName": username,
                "Password": to_md5(password),
                "ForceLogin": 1
            }
        })
        res = resp.json()
        return res['params']['Token']

    def queryRecords(self, startTime):
        """10．查询考勤记录

        :return:
        """

        params = dict(PersonCount=1000000)
        if startTime is not None and startTime > 0:
            params['StartTime'] = startTime

        url = 'http://{{ip}}/api/faceId/queryRecords'.replace('{{ip}}', self.__host)
        resp = requests.post(url=url, json={
            "params": params,
            "Token": self.__token
        })
        return resp.json()['params']

    def loadPersonsByDepartID(self):
        """2．人员的查询"""
        url = 'http://{{ip}}/api/person/loadPersonsByDepartID'.replace('{{ip}}', self.__host)
        resp = requests.post(url=url, json={
            "params": {
                "PersonCount": 1000
            },
            "Token": self.__token
        })
        return resp.json()['params']

    def loadPersonPicture(self, PersonID):
        """11．获取图片"""
        url = 'http://{{ip}}/api/person/loadPersonPicture'.replace('{{ip}}', self.__host)
        resp = requests.post(url=url, json={
            "params": {"PersonID": PersonID},
            "Token": self.__token
        })
        return resp.json()['params']

    def loadRecordPicture(self, FileName):
        """11．获取抓拍图片"""
        url = 'http://{{ip}}/api/faceId/loadRecordPicture'.replace('{{ip}}', self.__host)
        resp = requests.post(url=url, json={
            "params": {"FileName": FileName},
            "Token": self.__token
        })
        return resp.json()['params']

    def loadCompanyConfig(self):
        """获取部门信息"""
        url = 'http://{{ip}}/api/system/loadCompanyConfig'.replace('{{ip}}', self.__host)
        resp = requests.post(url=url, json={
            "params": {},
            "Token": self.__token
        })
        return resp.json()['params']


def timestamp_to_str(stamp):
    if len(str(stamp)) >= 13:
        stamp = float(str(stamp)[:10])
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(stamp))


if __name__ == '__main__':
    device_list = [
        dict(host='192.168.3.241'),
        dict(host='192.168.3.242'),
        dict(host='192.168.3.243'),
        dict(host='192.168.3.244'),
        dict(host='192.168.3.245'),
        dict(host='192.168.3.246'),
        dict(host='192.168.3.247'),
        dict(host='192.168.3.248'),
        dict(host='192.168.3.249'),
        dict(host='192.168.3.250'),
        dict(host='192.168.3.251'),
        dict(host='192.168.3.252'),
    ]

    hosts = []
    for device in device_list:
        hosts.append(device['host'])
    print(json.dumps(hosts))
    device_host = '192.168.3.241'
    persons = dict()  # type: dict[str, dict]
    records = dict()  # type: dict[int, CkRecord]
    for device in device_list:
        ckApi = CheckingApi(host=device['host'], username='admin', password='123456')
        rd = ckApi.loadCompanyConfig()
        logger.info(rd)
        rs = ckApi.queryRecords()
        for r in rs['Records']:
            ckp = CkRecord(host=device['host'], **r)
            rp = ckApi.loadRecordPicture(FileName=ckp.RecordPicture)
            try:
                ckp.RecordPicture = rp['Picture']
            except Exception as ex:
                pass
            if ckp.RecordID in records:

                logger.info("%s, %s, %s, %s, %s, %s, %s", device['host'], ckp.RecordID, ckp.PersonID, ckp.PersonName,
                            ckp.Similarity, timestamp_to_str(ckp.RecordTime), r)
                ckp = records[ckp.RecordID]
                logger.info('%s, %s, %s, %s, %s, %s, %s', ckp.host, ckp.RecordID, ckp.PersonID, ckp.PersonName,
                            ckp.Similarity, timestamp_to_str(ckp.RecordTime), r)
            else:
                records[ckp.RecordID] = ckp
                # print(ckp.RecordID, ckp.PersonID, ckp.PersonName, ckp.Similarity, r)

        rs = ckApi.loadPersonsByDepartID()
        for r in rs[u'Persons']:
            ckPer = CkPerson(**r)
            print (ckPer)
            personID = ckPer.PersonID
            if personID in persons:
                if persons[personID] == r:
                    pass
                else:
                    pass
            else:
                persons[personID] = r
            rp = ckApi.loadPersonPicture(PersonID=personID)
            # print (rp)
            try:
                # print(r['DepartID'], r['PersonID'], rp['Picture'])
                ckPer.Picture = rp['Picture']
                data = ckPer.__dict__
                pass
            except KeyError as e:
                print(r['DepartID'], r['PersonID'], r['PersonName'], rp['errMsg'])
            except Exception as e:
                print (e)
        break
