#!coding:utf8
import json
import winsound
import sys
sys.path.append("..")

from driver import *

class OrderDriver(IOTOSDriverI):

	valtest = ''
	# #1、通信初始化
	# def InitComm(self,attrs):
	# 	pass
    #
	#2、采集
	def Collecting(self, dataId):
		return self.valtest


	#3、控制
	#事件回调接口，其他操作访问
	def Event_customBroadcast(self, fromUuid, type, data):
		'''*************************************************

		TODO 

		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})


	# 事件回调接口，监测点操作访问
	def Event_getData(self, dataId, condition):
		return json.dumps({'code':0, 'msg':'', 'data':condition})


	# 事件回调接口，监测点操作访问
	def Event_setData(self, dataId, value):

		winsound.Beep(1000,500)

		# print self.id(u'虚拟设备B.t2')
		# print self.id(u'实际设备A1.string2')
		# print self.name('98673c9a.0ed163de')
		# print self.name('98673c9a.104c5a9f')

		if self.name(dataId) == 't2':
			self.valtest = self.value(u'实际设备B.f2') * 100000000 + self.value(u'实际设备B.f1')*1000 + int(value)
			self.setValue('test',self.valtest)

		return json.dumps({'code':0, 'msg':'', 'data':''})


	# 事件回调接口，监测点操作访问
	def Event_syncPubMsg(self, point, value):

		'''*************************************************

		TODO 

		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})