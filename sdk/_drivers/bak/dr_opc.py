#!coding:utf8
import json
import winsound
import sys

sys.path.append("..")
import OpenOPC
from driver import *


class OPCDriver(IOTOSDriverI):

	opc_collect = None
	opc_server = None

	#1、通信初始化
	def InitComm(self,attrs):
		#super(OPCDriver,self).initComm()
		#print config
		# try:

		self.opc_server = self.sysAttrs['config']['param']['server']
		if self.opc_collect is not None:
			self.opc_collect.close()
		self.opc_collect = OpenOPC.client()
		#print self.opc.servers() 	 														# 列出本机中所有opc server清单
		self.opc_collect.connect(self.opc_server)  											# 从opc server清单中选择需要连接的服务
		print self.sysAttrs['name'], "通信已连接！"

		# except Exception, e:
		# 	print e.message

	#2、采集
	def Collecting(self, dataId):
		return json.loads(self.Event_getData(dataId))['data']

	#3、控制
	#事件回调接口，其他操作访问
	def Event_customBroadcast(self, fromUuid, type, data):

		# tabletmp = {}
		# tabletmp[self.sysId] = self.sysAttrs
		# tabletmp[self.sysId]['data'] = self.data2Attrs
		# print json.dumps(tabletmp),'\r\n'

		return json.dumps({'code':0, 'msg':'', 'data':''})


	# 事件回调接口，监测点操作访问
	def Event_getData(self, dataId, condition = ''):
		configtmp = self.data2attrs[dataId]['config']
		addrtmp = configtmp['param']['addr']
		valtmp = None
		data = ''
		try:
			valtmp = self.opc_collect.read(addrtmp, sync=True)
			data = valtmp[0]
		except:
			#traceback.print_exc()
			print addrtmp + u' 读取失败 ' + str(valtmp)
		return json.dumps({'code':0, 'msg':'', 'data':data})


	# 事件回调接口，监测点操作访问
	def Event_setData(self, dataId, value):

		winsound.Beep(1000,300)

		try:
			#发现OPC在不同线程下，实例不能共用！！！
			self.opc_server = self.sysAttrs['config']['param']['server']
			addrtmp = self.data2attrs[dataId]['config']['param']['addr']
			opctmp = OpenOPC.client()
			opctmp.connect(self.opc_server)
			print opctmp.write((addrtmp,value))
			opctmp.close()
		except:
			traceback.print_exc()

		return json.dumps({'code':0, 'msg':'', 'data':''})


	# 事件回调接口，监测点操作访问
	def Event_syncPubMsg(self, point, value):
		'''*************************************************

		TODO 

		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})