# coding=utf-8
"""IOTOS官方标准MQTT协议驱动"""
import sys
import uuid as _uuid
import json
import time
import threading
from datetime import datetime
import paho.mqtt.client as mqtt

sys.path.append("..")
from driver import *
from library.log_utils import new_logger, logging
from library.dto import DataDto as _Data
logging.getLogger('iotos_sdk').setLevel(logging.ERROR)
logger = new_logger(name='iotos_mqtt')
logger.setLevel(level=logging.ERROR)
lock_logger = new_logger(name='LOCK')
lock_logger.setLevel(level=logging.INFO)



class Data(_Data):
    gateway_id = None  # type: int
    device_id = None  # type: int
    requestId = None  # type: str
    requestTime = None  # type: float
    responseId = None  # type: str
    responseTime = None  # type: float
    responseCode = None  # type: float


class IOTOSMQTT(object):
    __host = None  # type: str
    __port = None  # type: int
    __username = None  # type: str
    __password = None  # type: str
    __device_id = None  # type: int
    __gateway_id = None  # type: int

    on_update = None  # type: function # 接收设备端数据上报
    on_result = None  # type: function # 接收平台端下发到设备端，接收设备端处理结果反馈

    def __init__(self, host, port, username, password, device_id, gateway_id, on_update, on_result):
        self.__host = host
        self.__port = port
        self.__username = username
        self.__password = password
        self.__device_id = device_id
        self.__gateway_id = gateway_id
        self.on_update = on_update
        self.on_result = on_result
        logger.info('u=%s,p=%s', host, port)
        self.__connect()

    @property
    def update_topic(self):
        """数据上报topic"""
        return '$IOTOS/device/platform/update/%s/%s' % (self.__gateway_id, self.__device_id)

    def __connect(self):
        client_id = 'driver_%s_%s' % (self.__gateway_id, self.__device_id)
        self.__client = mqtt.Client(client_id=client_id, clean_session=True)
        self.__client.username_pw_set(username=self.__username, password=self.__password)
        self.__client.on_connect = self.on_connect
        self.__client.on_disconnect = self.on_disconnect
        self.__client.on_message = self.on_message

        rs = self.__client.connect(host=self.__host, port=self.__port)
        if rs == 0:
            logger.info('mqtt connected')
            self.__client.loop_start()
        else:
            logger.error('mqtt error, code=%s', rs)

    def on_message(self, client, userdata, message):
        """
        @type client: mqtt.Client
        @param userdata:
        @type message: mqtt.MQTTMessage
        @return:
        """
        if message.topic == self.update_topic:
            try:
                payload = json.loads(message.payload)  # type: dict[str, dict]
            except Exception as ex:
                logger.error('', exc_info=True)
                return
            if self.on_update is None:
                logger.error('on_update 没设置回调', exc_info=True)
                return
            # lock_logger.info('%s, time:%3.f, %s', payload['requestId'], time.time() - payload['requestTime'] / 1000, datetime.now())
            for data_oid, item in payload['data'].items():
                data = Data(**item)
                self.on_update(data)
        elif message.topic.startswith('$IOTOS/device/endpoint/update/%s/%s/' % (self.__gateway_id, self.__device_id)) and message.topic.endswith('/result'):
            lock_logger.info('%s, %s', message.topic, message.payload)
            item = json.loads(message.payload)
            data = Data(**item)
            self.on_result(data)

    def on_connect(self, client, userdata, flags, reasonCode, properties=None):
        logger.info(client)
        self.__client.subscribe(self.update_topic)
        result_topic = '$IOTOS/device/endpoint/update/%s/%s/+/result' % (self.__gateway_id, self.__device_id)
        rp = self.__client.subscribe(result_topic)
        logger.info('result.topic, %s', rp)

    def on_disconnect(self, client, userdata, reasonCode, properties=None):
        logger.error(client)
        rs = self.__client.reconnect()
        if rs != 0:
            logger.error('reconnect, code=%s', rs)

    def publish(self, data):
        """数据下发

        @type data: Data
        """
        topic = '$IOTOS/device/endpoint/update/%s/%s/%s' % (data.gateway_id, data.device_id, data.id)
        payload = dict(
            id=data.id,
            oid=data.data_oid,
            name=data.name,
            value=data.value,
            ts=time.time() * 1000,
            requestId=data.requestId and data.requestId or _uuid.uuid4().__str__().replace('-', ''),
            requestTime=time.time() * 1000
        )
        msg = self.__client.publish(topic=topic, payload=json.dumps(payload))  # type: mqtt.MQTTMessageInfo
        return msg


class IOTOSMQTTDriver(IOTOSDriverI):
    """IOTOS官方标准MQTT协议驱动"""

    __service = None
    __onLocks = None  # type: dict[str, threading.Event]
    __onResults = None  # type: dict[str, Data]

    # 通讯初始化
    def InitComm(self, attrs=None):
        self.pauseCollect = True
        self.__service = IOTOSMQTT(username=self.zm.username, password=self.zm.password, host=self.zm.server_ip,
                                   port=1883, device_id=self.sysAttrs['id'], gateway_id=self.sysAttrs['gateway_id'],
                                   on_result=self.__on_result, on_update=self.__on_update)
        self.online(True)
        self.__onLocks = {}
        self.__onResults = {}
        threading.Thread(target=self.test_setData).start()

    def test_setData(self):

        for i in range(0, 100000):
            threading.Thread(target=self.Event_setData, args=('7be6', i, )).start()
            time.sleep(1)

    def __on_result(self, data):
        """接收平台下发数据到设备端的处理结果

        @type data: Data
        """
        lock_logger.info('%s, has:%s, time:%3.f', data.requestId, data.requestId in self.__onLocks, (data.responseTime - data.requestTime)  / 1000)
        if data.requestId in self.__onLocks:
            self.__onResults[data.requestId] = data
            lock_logger.info('%s, event unlock, %s', data.requestId, datetime.now())
            self.__onLocks[data.requestId].set()

    def __on_update(self, data):
        """

        @type data: Data
        """
        if data.oid in self.data2attrs:
            rs = self.setValue(name=data.name, value=data.value, timestamp=data.timestamp)
            if rs['code'] != 0:
                logger.error('setValue(%s, %s, %s), rs=%s', data.name, data.value, data.timestamp, rs)
        else:
            logger.warning('dataId=%s, 数据点不存在')

    # 设备数据下发
    def Event_setData(self, dataId, value):
        # lock_logger.info('setData, %s=%s', dataId, value)
        if dataId in self.data2attrs:
            data = dict(device_id=self.sysAttrs['id'], gateway_id=self.sysAttrs['gateway_id'])
            data.update(**self.data2attrs[dataId])
            data = Data(**data)
            data.value = value
            data.requestId = _uuid.uuid4().__str__().replace('-', '')
            rs = self.__service.publish(data=data)
            lock_logger.info('%s, event publish, %s', data.requestId, datetime.now())
            if rs.rc != 0:
                lock_logger.error('mqtt.publish.code=%s', rs.rc)
                return json.dumps({'code': rs.rc, 'msg': '', 'data': ''})
            event = self.__onLocks[data.requestId] = threading.Event()
            lock_logger.info('%s, event lock, %s', data.requestId, datetime.now())
            event.clear()
            lock_logger.info('%s, event wait, %s', data.requestId, datetime.now())
            event.wait(100)
            lock_logger.info('%s, event timeout, %s', data.requestId, datetime.now())
            del self.__onLocks[data.requestId]
            event = None
            if data.requestId in self.__onResults:
                rs_data = self.__onResults[data.requestId]
                del self.__onResults[data.requestId]
                lock_logger.info('%s, %s, %s', data.requestId, rs_data, datetime.now())
                lock_logger.info('%s, %s, %s', data.requestId, (rs_data.responseTime - rs_data.requestTime) / 1000, datetime.now())
                return json.dumps({'code': rs_data.responseCode, 'msg': rs_data.responseCode, 'data': ''})
            else:
                lock_logger.info('%s, %s, %s', data.requestId, 'not result', datetime.now())
        return json.dumps({'code': 502, 'msg': 'mqtt device response timeout', 'data': ''})
