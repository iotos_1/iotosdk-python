#!coding:utf8
import json
import sys
sys.path.append("..")
from opcua import Client, ua
from driver import *
import time
class OPCua():
	def __init__(self, _driver_instance):
		self.driver_instance = _driver_instance


	def datachange_notification(self, node, val, data):
		print("Python: New data change event", val)



class TemplateDriver(IOTOSDriverI):
	#1、通信初始化
	def InitComm(self,attrs):
		self.setPauseCollect(False)
		self.setCollectingOneCircle(False)
		self.online(True)
		self.apartment = OPCua(self)
        #这里将设备参数写上连接的server地址，这里硬件商或客户应知道opc ua的server地址
		self.client = Client('{}'.format(self.sysAttrs['config']['param']['opcua']), timeout=10)
		self.client.connect()

	#2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
	def Collecting(self, dataId):
		try:
			cfgtmp = self.data2attrs[dataId]['config']
			# 过滤掉非采集的点，存在采集的node，则在param中的node写上ns;s或者其他参数
			if cfgtmp["param"] == "" :
				return ()

			# 过滤采集点
			if 'disabled' in cfgtmp['param'] and cfgtmp['param']['disabled'] == True:
				return ()
			else:
				self.warn(self.name(dataId))

            #读取采集点配置参数，这里可将采集点的时间周期缩短
			node = cfgtmp['param']['node']
			print(node)
            #这里的ns是需要去opc ua 查看并填写内容的
			dataId_num = self.client.get_node('{}={};{}={}'.format(cfgtmp['param']['nodeid_name'],cfgtmp['param']['nodeid'],cfgtmp['param']['node_name'],cfgtmp['param']['node']))
			handler = self.apartment
			sub = self.client.create_subscription(500, handler)
			sub.subscribe_data_change(dataId_num)
			ret_nu = dataId_num.get_value()
			return (ret_nu,)
		except Exception as e:
			print(e)




	#3、控制
	#广播事件回调，其他操作访问
	def Event_customBroadcast(self, fromUuid, type, data):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	# 4、查询
	# 查询事件回调，数据点查询访问
	def Event_getData(self, dataId, condition):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	# 5、控制事件回调，数据点控制访问
	def Event_setData(self, dataId, value):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})

	# 6、本地事件回调，数据点操作访问
	def Event_syncPubMsg(self, point, value):
		'''*************************************************
		TODO 
		**************************************************'''
		return json.dumps({'code':0, 'msg':'', 'data':''})