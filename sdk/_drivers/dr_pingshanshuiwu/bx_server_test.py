# coding=utf-8
"""碧兴我院-设备服务器端测试"""
import os
tz_code = 'Asia/Shanghai'
os.environ['TZ'] = tz_code
import traceback
import pytz

tz = pytz.timezone(tz_code)
import logging
import signal
import sys
sys.path.insert(0, os.path.dirname(os.getcwd()))
import threading
from datetime import datetime

log_format = '%(asctime)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=log_format)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
from dr_pingshanshuiwu.bx_device import DeviceServer, CallbackI


class Callback(CallbackI):

    def on_response(self, data):
        logger.debug(data)


if __name__ == '__main__':
    server = DeviceServer(host='0.0.0.0', port=3882, callbackClass=Callback())
    while True:
        pass
