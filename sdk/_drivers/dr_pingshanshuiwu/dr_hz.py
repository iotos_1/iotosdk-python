# coding=utf-8
"""碧兴物联 SL651 TCP 3882"""
import os
import json
import sys

sys.path.append(os.getcwd())
from driver import *
logger.setLevel(logging.DEBUG)
from .bx_device import DeviceServer, SL651CallbackI
from .SL651_2014 import SL651Response
from .service import Service
import logging
logging.getLogger(name='iotos_sdk').setLevel(logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class HZDriver(IOTOSDriverI, SL651CallbackI):
    # 1、通信初始化
    __server = None # type: DeviceServer
    __service = None # type: Service

    def InitComm(self, attrs):
        self.setPauseCollect(True)
        self.setCollectingOneCircle(False)
        # self.online(True)
        # self.__service = Service(datas=attrs['data'])
        # try:
        #     port = attrs['config']['port']
        # except KeyError:
        #     port = 3881
        # self.__server = DeviceServer(host='0.0.0.0', port=port, callbackClass=self)

    def on_response(self, data):
        """接收设备上报

                :@type data: SL651Response
                """
        logger.debug(data)
        # 转换数据
        new_data = self.__service.make_bx(data)

        # 上报数据
        for k, v in new_data.items():
            try:
                r = self.setValue(name=k, value=v)
                logger.info('%s=>%s=>%s', k, v, r)
            except Exception as ex:
                logger.error(ex)


    # 2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
    def Collecting(self, dataId):
        '''*************************************************
        TODO
        **************************************************'''
        return 1

    # 3、控制
    # 广播事件回调，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 4、查询
    # 查询事件回调，数据点查询访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 5、控制事件回调，数据点控制访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 6、本地事件回调，数据点操作访问
    def Event_syncPubMsg(self, point, value):

        self.warn(point + " ==> " + value)
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
