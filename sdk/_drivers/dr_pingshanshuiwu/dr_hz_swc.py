# coding=utf-8
"""碧兴物联 SL651 TCP 3882"""
import os
import json
import sys

sys.path.append(os.getcwd())
from driver import *
from .bx_device import DeviceServer, SL651CallbackI
from .SL651_2014 import SL651Protocol, SL651Response
from .service import Service

class HZswcDriver(IOTOSDriverI):
    # 1、通信初始化
    __server = None # type: DeviceServer
    __service = None # type: Service

    def InitComm(self, attrs):
        self.setPauseCollect(True)
        self.setCollectingOneCircle(False)
        self.__service = Service(datas=attrs['data'])

    # 2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
    def Collecting(self, dataId):
        '''*************************************************
        TODO
        **************************************************'''
        return ()

    # 3、控制
    # 广播事件回调，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 4、查询
    # 查询事件回调，数据点查询访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 5、控制事件回调，数据点控制访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 6、本地事件回调，数据点操作访问
    def Event_syncPubMsg(self, point, value):
        # 转换数据
        new_data = self.__service.make_bx(value)

        self.warn(new_data['yczAddress'])
        self.warn(self.sysAttrs['config']['param']['yczAddress'])

        addressMatched = new_data['yczAddress'] == self.sysAttrs['config']['param']['yczAddress']

        # self.online(addressMatched)

        if addressMatched:
            # 上报数据
            for k, v in new_data.items():
                print(k,v)
                try:
                    r = self.setValue(name=k, value=v)
                    logger.info('%s=>%s=>%s', k, v, r)
                except Exception as ex:
                    logger.error(ex)
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
