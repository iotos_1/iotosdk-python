# coding=utf-8
"""HJ212-2017"""
import os
import sys
import json
import logging

logger = logging.getLogger(__name__)


def crc16(text):
    data = bytearray(text, encoding='utf-8')
    crc = 0xffff
    dxs = 0xa001
    for i in range(len(data)):
        hibyte = crc >> 8
        crc = hibyte ^ data[i]
        for j in range(8):
            sbit = crc & 0x0001
            crc = crc >> 1
            if sbit == 1:
                crc ^= dxs
    return hex(crc)[2:]


class HJ212Header(object):
    """报头

    :param centre: 中心站地址
    """
    # param centre: 中心站地址
    centre = None  # type: str # 中心站地址
    pwd = None  # type: str # 密码
    yczAddress = None  # type: str
    functionCode = None  # type: str
    msgId = None  # type: str
    yczType = None  # type: str
    observationTime = None  # type: int
    dataLen = None  # 数据报文长度

    def __str__(self):
        return json.dumps(self.__dict__)


class HJ212Response(object):
    header = None  # type: HJ212Header
    data = None  # type: dict
    dataList = None  # type: [dict]

    def __str__(self):
        return json.dumps(dict(
            header=self.header.__dict__,
            data=self.data,
            dataList=self.dataList
        ))


class HJ212Protocol(object):

    def decode(self, data_bin):
        assert isinstance(data_bin, bytes), 'data is not bytes'
        data = data_bin.decode('UTF-8')
        DIC_HJ212_2017 = {}
        DIC_HJ212_2017['HEAD'] = data[0:2]
        DIC_HJ212_2017['LENGTH'] = data[2:6]
        DIC_HJ212_2017['CRC'] = data[-6:-2]

        DIC_HJ212_2017['DATA'] = {}
        DIC_HJ212_2017['DATA']['CP'] = {}

        _d0 = data[6:-6].split('&&')

        _d1 = _d0[0][0:-4].split(';')
        for _d2 in _d1:
            _d3 = _d2.split('=')
            DIC_HJ212_2017['DATA'][_d3[0]] = _d3[1]

        _d4 = _d0[1].split(';')
        for _d5 in _d4:
            _d6 = _d5.split(',')
            for _d7 in _d6:
                _d8 = _d7.split('=')
                DIC_HJ212_2017['DATA']['CP'][_d8[0]] = _d8[1]
        _data = data[6:-6]

        if (int(DIC_HJ212_2017['LENGTH']) == len(_data)):
            logger.debug('数据长度验证通过')

        else:
            logger.debug('数据长度验证未通过')

        if (DIC_HJ212_2017['CRC'] == str(crc16(_data)).zfill(4).upper()):
            logger.debug('CRC校验通过')
        else:
            logger.debug('CRC校验未通过')

            # print(DIC_HJ212_2017)  调试输出 结构为双层字典，类似JAVA对象
        return DIC_HJ212_2017
