# coding=utf-8
"""上海航征 SL651 TCP 3881"""
import os
import json
import sys
import time

from routelib.ice_connent import IceService

sys.path.append(os.getcwd())
from driver import *

logger.setLevel(logging.DEBUG)
from .bx_device import DeviceServer as SL651DeviceService, SL651CallbackI
from .SL651_2014 import SL651Response
from .service import Service
from .rpc_service import DeviceService, RPCService
import logging

logging.getLogger(name='iotos_sdk').setLevel(logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class BXDriver(IOTOSDriverI, SL651CallbackI):
    # 1、通信初始化
    __server = None  # type: SL651DeviceService
    __service = None  # type: Service
    __deviceService = None  # type: DeviceService

    def InitComm(self, attrs):
        self.setPauseCollect(True)
        self.setCollectingOneCircle(False)
        self.online(True)
        '''*************************************************
        TODO
        #self.online(True)
        #self.setValue(u'demo_device.热水供水泵控制', True)
        **************************************************'''
        rpc = RPCService(rpc=self.zm.iceService, table=self.zm.m_table)
        # self.__deviceService = DeviceService(rpc=rpc)

        # res = rpc.publish('指定设备名称', value_map={
        #     'observationTime': int(time.time()),
        #     'msgId': 'xxxxx'
        # })

        res = rpc.publish(attrs['name'], value_map={
            'observationTime': int(time.time()),
            'msgId': 'xxxxx'
        })
        logger.debug(res)
        self.__service = Service(datas=attrs['data'])
        try:
            port = attrs['config']['port']
        except KeyError:
            port = 3882
        self.__server = SL651DeviceService(host='0.0.0.0', port=port, callbackClass=self)

    def on_response(self, data):
        """接收设备上报

        :@type data: SL651Response
        """
        logger.debug(data)
        # 转换数据
        new_data = self.__service.make_bx(data)

        # 上报数据
        for k, v in new_data.items():
            try:
                r = self.setValue(name=k, value=v)
                logger.info('%s=>%s=>%s', k, v, r)
            except Exception as ex:
                logger.error(ex)

    # 2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
    def Collecting(self, dataId):
        '''*************************************************
        TODO
        **************************************************'''
        return 1

    # 3、控制
    # 广播事件回调，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 4、查询
    # 查询事件回调，数据点查询访问
    def Event_getData(self, dataId, condition):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 5、控制事件回调，数据点控制访问
    def Event_setData(self, dataId, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 6、本地事件回调，数据点操作访问
    def Event_syncPubMsg(self, point, value):
        '''*************************************************
        TODO
        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})
