# coding=utf-8
"""SL651-2014"""
import json
import logging
import sys
from binascii import unhexlify

from crcmod import crcmod

logger = logging.getLogger(__name__)


class ProtocolData(object):
    pass


'''
    crc16效验

'''


def crc16_modbus(read):
    crc16 = crcmod.mkCrcFun(0x18005, rev=True, initCrc=0xFFFF, xorOut=0x0000)
    data = read
    readcrcout = hex(crc16(unhexlify(data[0:-4]))).upper()
    str_list = list(readcrcout)
    logger.debug(str_list)
    return ''.join(str_list[2:]).upper()


class SL651Header(object):
    """SL651协议头部

    :param centre: 中心站地址
    """
    # param centre: 中心站地址
    centre = None  # type: str # 中心站地址
    pwd = None  # type: str # 密码
    yczAddress = None  # type: str
    functionCode = None  # type: str
    msgId = None  # type: str
    yczType = None  # type: str
    observationTime = None  # type: int
    dataLen = None  # 数据报文长度

    def __str__(self):
        return json.dumps(self.__dict__)


class SL651Response(object):
    header = None  # type: SL651Header
    data = None  # type: dict
    dataList = None  # type: [dict]

    def __str__(self):
        return json.dumps(dict(
            header=self.header.__dict__,
            data=self.data,
            dataList=self.dataList
        ))


class SL651Protocol(object):
    """SL651-2014协议解禁"""

    def decode(self, data):
        """32定时报

        :param data: bytes数据包
        :type data: bytes
        :rtype: SL651Response
        """
        assert isinstance(data, bytes), 'data is not bytes'
        logger.debug(data[0])
        if hasattr(data, 'hex'):
            _hexs = data.hex().upper()
        else:
            _hexs = data.upper().decode('UTF-8')
        data_hexs = []
        while len(_hexs):
            data_hexs.append(_hexs[0:2])
            _hexs = _hexs[2:]
        logger.debug('data.hex:%s', data_hexs)
        logger.debug(data_hexs[0:2])
        if ''.join(data_hexs[0:2]) == '7E7E':
            logger.debug('HEX')
            data_hexs.pop(0)
            data_hexs.pop(0)
        _heaer = SL651Header()
        # 中心站地址,1字节
        centre = data_hexs.pop(0)
        logger.debug('中心站地址:%s', centre)
        _heaer.centre = centre
        # 遥测站地址,5字节
        ycz = ' '.join(data_hexs[0:5])
        logger.debug('遥测站地址:%s', ycz)
        data_hexs = data_hexs[5:]
        _heaer.yczAddress = ycz

        # 密码， 2字节
        pwd = ' '.join(data_hexs[0:2])
        logger.debug('密码:%s', pwd)
        data_hexs = data_hexs[2:]
        _heaer.pwd = pwd

        fun_code = data_hexs.pop(0)
        logger.debug('功能码:%s', fun_code)
        _heaer.functionCode = fun_code

        data_info = data_hexs[0:2]
        data_len = int(data_info[1], 16)
        logger.debug('报文上下行标识及长度:%s, 上下行标识：%s, 报文长度：%s', data_info, data_info[0], data_len)
        data_hexs = data_hexs[2:]
        _heaer.dataLen = data_len
        # 报文起始符, 固定值：02H
        dataStartTag = data_hexs[0]
        if dataStartTag != '02':
            raise ValueError('报文结构naip')
        # 移除报文起始标识
        data_hexs.pop(0)
        # 提取报文
        data_body = data_body = data_hexs[0: data_len]  # type: [str]
        logger.debug('报文：%s', data_body)
        # 流水号, 2字节
        number = data_body[0:2]
        logger.debug('流水号:%s->%s', number, int(''.join(number), 16))
        data_body = data_body[2:]
        _heaer.msgId = number

        # 发报时间, 6字节BCD码，YYMMDDHHmmSS
        pusb_date = data_body[0:6]
        logger.debug('发报时间:%s', pusb_date)
        data_body = data_body[6:]

        if data_body[0] == 'F1' and data_body[1] == 'F1':
            data_body.pop(0)
            data_body.pop(0)
            _F1F1 = data_body[0:5]
            data_body = data_body[5:]
        else:
            raise ValueError('报文不合法')
        logger.debug('遥测站地址.地址标识符:%s, 遥测站地址:%s', 'F1F1', _F1F1)

        # 遥测站分类码, 1字节
        yczType = data_body.pop(0)
        logger.debug('遥测站分类码:%s', yczType)
        _heaer.yczType = yczType
        # 观测时间, 5字节BCD码，YYMMDDHHmm
        if data_body[0] == 'F0' and data_body[1] == 'F0':
            data_body.pop(0)
            data_body.pop(0)
            _F0F0 = data_body[0:5]
            data_body = data_body[5:]
            logger.debug('F0F0:%s', _F0F0)
            _heaer.observationTime = '20{}-{}-{} {}:{}'.format(*_F0F0)
        else:
            raise ValueError('报文不合法')
        logger.debug('观测时间.地址标识符:%s, 观测时间:%s', 'F0F0', _F0F0)
        logger.debug(data_body)
        # 报文结尾
        logger.debug(data_hexs[data_len:])

        data_map = dict()
        while len(data_body):
            is_ff = False
            _tag = data_body.pop(0)
            if _tag == 'FF':
                is_ff = True
                _tag = _tag + '_' + data_body.pop(0)

            _data = data_body.pop(0)
            logger.debug('tag=%s, data=%s', _tag, _data)
            _bin = bin(int(_data, 16))
            _bin = _bin[2:]
            # 小数点索引
            _point = _bin[-3:]  # 000
            _len = _bin[0:-3]  # 000
            logger.debug(('data', _len, _point))
            _len = int(_len, 2)
            _point = int(_point, 2)
            logger.debug(('data', _len, _point))
            tag_data = data_body[0:_len]  # type: [str]
            data_body = data_body[_len:]
            logger.debug('tag:%s, raw data:%s', _tag, tag_data)
            # 处理有小数点的
            tag_value = ''.join(tag_data)
            if _point != 0:
                tag_value = [v for v in tag_value]
                logger.debug('tag_value:%s', tag_value)
                tag_value.reverse()
                tag_value.insert(_point, '.')
                tag_value.reverse()
                logger.debug('tag_value:%s', tag_value)
                # 添加小数后，重新组合
                tag_value = ''.join(tag_value)
                logger.debug('tag:%s, data:%s', _tag, tag_value)
            logger.debug('tag=%s,len=%s->%s->%s', _tag, _data, int(_data, 16), bin(int(_data, 16)))
            logger.debug('data_body:%s', data_body)
            # if is_ff:
            #     tag_value = F'-{float(tag_value)}'
            data_map[_tag] = float(tag_value)
        _response = SL651Response()
        _response.header = _heaer
        _response.data = data_map
        return _response

    def decode2(self, data):
        """32定时报

        :param data: bytes数据包
        :type data: bytes
        :rtype: SL651Response
        """
        data_bin = data
        assert isinstance(data, bytes), 'data is not bytes'
        logger.debug(data[0])
        if hasattr(data, 'hex'):
            _hexs = data.hex().upper()
        else:
            _hexs = data.upper().decode('UTF-8')

        data_hexs = []
        while len(_hexs):
            data_hexs.append(_hexs[0:2])
            _hexs = _hexs[2:]
        logger.debug('data.hex:%s', data_hexs)
        logger.debug(data_hexs[0:2])
        if ''.join(data_hexs[0:2]) == '7E7E':
            logger.debug('HEX')
            data_hexs.pop(0)
            data_hexs.pop(0)
        _heaer = SL651Header()
        # 中心站地址,1字节
        centre = data_hexs.pop(0)
        logger.debug('中心站地址:%s', centre)
        _heaer.centre = centre
        # 遥测站地址,5字节
        ycz = ' '.join(data_hexs[0:5])
        logger.debug('遥测站地址:%s', ycz)
        data_hexs = data_hexs[5:]
        _heaer.yczAddress = ycz

        # 密码， 2字节
        pwd = ' '.join(data_hexs[0:2])
        logger.debug('密码:%s', pwd)
        data_hexs = data_hexs[2:]
        _heaer.pwd = pwd

        fun_code = data_hexs.pop(0)
        logger.debug('功能码:%s', fun_code)
        _heaer.functionCode = fun_code

        data_info = data_hexs[0:2]
        data_len = int(data_info[1], 16)
        logger.debug('报文上下行标识及长度:%s, 上下行标识：%s, 报文长度：%s', data_info, data_info[0], data_len)
        data_hexs = data_hexs[2:]
        _heaer.dataLen = data_len
        # 报文起始符, 固定值：02H
        dataStartTag = data_hexs[0]
        if dataStartTag != '02':
            raise ValueError('报文结构不全法')
        # 移除报文起始标识
        data_hexs.pop(0)
        # 提取报文
        data_body = data_body = data_hexs[0: data_len]  # type: [str]
        logger.debug('报文：%s', data_body)
        # 流水号, 2字节
        number = data_body[0:2]
        logger.debug('流水号:%s->%s', number, int(''.join(number), 16))
        data_body = data_body[2:]
        _heaer.msgId = number

        # 发报时间, 6字节BCD码，YYMMDDHHmmSS
        pusb_date = data_body[0:6]
        logger.debug('发报时间:%s', pusb_date)
        data_body = data_body[6:]

        if data_body[0] == 'F1' and data_body[1] == 'F1':
            data_body.pop(0)
            data_body.pop(0)
            _F1F1 = data_body[0:5]
            data_body = data_body[5:]
        else:
            raise ValueError('报文不合法')
        logger.debug('遥测站地址.地址标识符:%s, 遥测站地址:%s', 'F1F1', _F1F1)

        # 遥测站分类码, 1字节
        yczType = data_body.pop(0)
        logger.debug('遥测站分类码:%s', yczType)
        _heaer.yczType = yczType
        # 观测时间, 5字节BCD码，YYMMDDHHmm
        if data_body[0] == 'F0' and data_body[1] == 'F0':
            pass
            # data_body.pop(0)
            # data_body.pop(0)
            # _F0F0 = data_body[0:5]
            # data_body = data_body[5:]
            # logger.debug('F0F0:%s', _F0F0)
            # _heaer.observationTime = '20{}-{}-{} {}:{}'.format(*_F0F0)
        else:
            raise ValueError('报文不合法')
        logger.debug(data_body)
        # 报文结尾
        logger.debug(data_hexs[data_len:])

        data_list = []  # type: [dict]
        data_map = dict()
        while len(data_body):
            if data_body[0] == 'F0' and data_body[1] == 'F0':
                if len(data_map) > 0:
                    data_list.append(data_map.copy())
                    data_map = dict()
                data_body.pop(0)
                data_body.pop(0)
                _F0F0 = data_body[0:5]
                data_body = data_body[5:]
                logger.debug('F0F0:%s', _F0F0)
                _heaer.observationTime = '20{}-{}-{} {}:{}'.format(*_F0F0)
                logger.debug('观测时间.地址标识符:%s, 观测时间:%s', 'F0F0', _F0F0)
            _tag = data_body.pop(0)
            if _tag == 'FF':
                is_ff = True
                _tag = _tag + '_' + data_body.pop(0)

            _data = data_body.pop(0)
            logger.debug('tag=%s, data=%s', _tag, _data)
            _bin = bin(int(_data, 16))
            _bin = _bin[2:]
            # 小数点索引
            _point = _bin[-3:]  # 000
            _len = _bin[0:-3]  # 000
            logger.debug(('data', _len, _point))
            _len = int(_len, 2)
            _point = int(_point, 2)
            logger.debug(('data', _len, _point))
            tag_data = data_body[0:_len]  # type: [str]
            data_body = data_body[_len:]
            logger.debug('tag:%s, raw data:%s', _tag, tag_data)
            # 处理有小数点的
            tag_value = ''.join(tag_data)
            if _point != 0:
                tag_value = [v for v in tag_value]
                logger.debug('tag_value:%s', tag_value)
                tag_value.reverse()
                tag_value.insert(_point, '.')
                tag_value.reverse()
                logger.debug('tag_value:%s', tag_value)
                # 添加小数后，重新组合
                tag_value = ''.join(tag_value)
                logger.debug('tag:%s, data:%s', _tag, tag_value)
            logger.debug('tag=%s,len=%s->%s->%s', _tag, _data, int(_data, 16), bin(int(_data, 16)))
            logger.debug('data_body:%s', data_body)
            # if is_ff:
            #     tag_value = F'-{float(tag_value)}'
            data_map[_tag] = float(tag_value)
        data_list.append(data_map)
        _response = SL651Response()
        _response.header = _heaer
        if len(data_list) > 1:
            _response.dataList = data_list
        else:
            _response.data = data_map
        return _response

    def eecode(self, response):
        """回复报文编码

        @type response: SL651Response
        @rtype: bytes
        """
        resp_hexs = ['7E7E']
        resp_hexs += response.header.yczAddress  # 遥测站地址
        resp_hexs += [response.header.centre]  # 中心站地址
        resp_hexs += response.header.pwd  # 密码
        resp_hexs += [response.header.functionCode]
        resp_hexs += ['{:x}'.format(0b10001000)]
        resp_hexs += ['02']
        resp_hexs += response.header.msgId
        resp_hexs += ['22', '05', '02', '15', '30']
        resp_hexs += ['04']
        resp_body = ''.join(resp_hexs).lower().replace(' ', '')
        crc16_code = crc16_modbus(resp_body)
        logger.debug(crc16_code)
        logger.debug(resp_body)
        logger.debug(resp_body + crc16_code)
        logger.debug(type(resp_body))

        resp_bin = bytes().fromhex(resp_body + crc16_code)
        return resp_bin
