# coding=utf-8
"""抽取通用功能代码"""
import logging

from .SL651_2014 import SL651Response
from datetime import datetime
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CallbackI(object):
    """统一回调接口"""

    def on_response(self, data):
        """

        @type data: dict
        @return:
        """
        raise NotImplemented()


def str_to_timestamp(date_time, format="%Y-%m-%d %H:%M:%S"):
    """时间戳转datetime

    :type date_time: str
    :param date_time: 2019-12-02 14:40:35
    :rtype: float
    """
    return datetime.strptime(date_time, format).timestamp()


class Service(object):
    """业务代码"""

    __hex_map = None  # type: dict
    __ascii_map = None  # type: dict
    __name_map = None  # type: [str]

    def __init__(self, datas):
        """

        @param datas: 数据点字典
        @type datas: dict[str, dict]
        """
        # 提取映谢数据
        hex_map = dict()  # type: dict[str, str]
        ascii_map = dict()  # type: dict[str, str]
        self.__name_map = []
        for data_oid, attr in datas.items():
            data_name = attr['name']
            self.__name_map.append(data_name)
            try:
                _hex = attr['config']['hex']
                hex_map[str(_hex)] = data_name
            except KeyError:
                _hex = None
            try:
                _ascii = attr['config']['ascii']
                ascii_map[_ascii] = data_name
            except KeyError:
                _ascii = None

        self.__hex_map = hex_map
        self.__ascii_map = ascii_map

    def __clena_keys(self, data):
        """清除不存在键值对

        @type data: dict
        @rtype: dict
        """
        new_data = dict()
        for k, v in data.items():
            if k in self.__name_map:
                new_data[k] = v
        return new_data

    def __make(self, data):
        """将SL651报文数据转换为珠水委数据-处理公共字段

        @type data: SL651Response
        @rtype: dict
        """
        new_data = dict()
        new_data['yczAddress'] = data.header.yczAddress
        new_data['functionCode'] = data.header.functionCode
        new_data['msgId'] = data.header.msgId
        new_data['yczType'] = data.header.yczType
        observationTime = data.header.observationTime
        observationTime = str_to_timestamp(observationTime, format='%Y-%m-%d %H:%M')
        new_data['observationTime'] = int(observationTime)
        return new_data

    def make_zsw(self, data):
        """将SL651报文数据转换为珠水委数据

        @type data: SL651Response
        @rtype: dict
        """
        new_data = self.__make(data=data)
        data_list = []
        for _data in data.dataList:
            _new_data = new_data.copy()
            for _hex, v in _data.items():
                if _hex in self.__hex_map:
                    data_name = self.__hex_map[_hex]
                    _new_data[data_name] = v
            _new_data = self.__clena_keys(data=_new_data)
            data_list.append(_new_data)
        return data_list

    def make_bx(self, data):
        """将SL651报文数据转换为珠水委数据

        @type data: SL651Response
        @rtype: dict
        """
        new_data = self.__make(data=data)
        logger.debug(self.__hex_map)
        for _hex, v in data.data.items():
            if _hex in self.__hex_map:
                data_name = self.__hex_map[_hex]
                new_data[data_name] = v
        new_data = self.__clena_keys(data=new_data)
        return new_data

    def make_qf(self, data):
        """HJ212报文数据转换为珠水委数据

        @type data: SL651Response
        @rtype: dict
        """
        new_data = dict()

        for _hex, v in data.items():
            if _hex in self.__hex_map:
                data_name = self.__hex_map[_hex]
                new_data[data_name] = v
        new_data = self.__clena_keys(data=new_data)
        return new_data
