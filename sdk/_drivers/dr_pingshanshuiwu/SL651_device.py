# coding=utf-8
"""SL651-2014-设备服务器"""
# coding=utf-8
"""碧兴物联相关代码"""
import os
tz_code = 'Asia/Shanghai'
os.environ['TZ'] = tz_code
import pytz

tz = pytz.timezone(tz_code)
import logging
import threading
from datetime import datetime

log_format = '%(asctime)s %(threadName)s %(levelname)s %(filename)s:%(lineno)d %(funcName)s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=log_format)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.getLogger('SL651_2014').setLevel(logging.INFO)

import socket
from .SL651_2014 import SL651Protocol, SL651Response


class CallbackI(object):
    """统一回调接口"""

    def on_response(self, data):
        """

        @type data: dict
        @return:
        """
        raise NotImplemented()


class SL651CallbackI(CallbackI):
    """统一回调接口"""

    def on_response(self, data):
        """

        @type data: SL651Response
        @return:
        """
        raise NotImplemented()


class SL651DeviceServer(object):
    """设备服务"""
    __callBack = None  # type: SL651CallbackI
    __server = None  #

    def __init__(self, host, port, callbackClass):
        """

        @param callbackClass: CallbackI
        """
        self.__callBack = callbackClass
        address = (host, port)
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.settimeout(86400)
        while True:
            try:
                server.bind(address)  # 绑定端口
                break
            except Exception as ex:
                pass
        server.listen(10)  # 监听
        logger.info('connected successed')
        self.__server = server
        t = threading.Thread(target=self.__loop_forever)
        t.setDaemon(True)
        t.start()

    def handle_request(self, client, addr):
        """

        @type client: socket.socket
        @param addr:
        @return:
        """
        logger.debug((client, addr))
        MaxBytes = 1024 * 1024
        while True:
            try:
                data_bin = client.recv(MaxBytes)
                if not data_bin:
                    logger.info('数据为空，我要退出了')
                    break
                logger.info('client:%s:%s, type:%s, len:%s, data: %s', addr[0], addr[1], type(data_bin), len(data_bin),
                            data_bin)
                try:
                    logger.info('client:%s:%s, type:%s, len:%s, data: %s', addr[0], addr[1], type(data_bin.hex()),
                                len(data_bin.hex()), data_bin.hex())
                except Exception as ex:
                    logger.error(ex)
                try:
                    response = SL651Protocol().decode2(data_bin)
                except Exception as ex:
                    logger.error('decode error', exc_info=1)
                    continue
                try:
                    resp_bin = SL651Protocol().eecode(response=response)
                    rs = client.send(resp_bin)
                    logger.info('rs:%s, %s', rs, resp_bin.hex())
                except Exception as ex:
                    logger.error(ex)
                if self.__callBack:
                    try:
                        self.__callBack.on_response(response)
                    except Exception as ex:
                        logger.error('callBack.on_response', exc_info=1)
                # client.send(data_bin)
            except Exception:
                logger.error("client error", exc_info=1)
                break

    def __loop_forever(self):
        try:
            while True:
                client, addr = self.__server.accept()  # 等待客户端连接a
                threading.Thread(target=self.handle_request, args=(client, addr,)).start()
                logger.info('%s, %s', addr, "连接上了")

        except BaseException as e:
            logger.info("出现异常：%s", datetime.now(), exc_info=1)
        except KeyboardInterrupt as e:
            logger.warning('用户关闭进程', exc_info=1)


SL651DeviceService = SL651DeviceServer


class UDPDeviceServer(object):
    """UDP设备服务"""
    __callBack = None  # type: SL651CallbackI
    __server = None  #

    def __init__(self, host, port, callbackClass):
        """

        @param callbackClass: CallbackI
        """
        self.__callBack = callbackClass
        address = (host, port)
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.settimeout(86400)
        while True:
            try:
                server.bind(address)  # 绑定端口
                break
            except Exception as ex:
                pass
        logger.info('connected successed')
        self.__server = server
        t = threading.Thread(target=self.__loop_forever)
        t.setDaemon(True)
        t.start()

    def handle_request(self, data, addr):
        """

        @type data: bytes
        @param addr:
        @return:
        """
        logger.debug((data, addr))
        MaxBytes = 1024 * 1024
        while True:
            try:
                data_bin = data
                if not data_bin:
                    logger.info('数据为空，我要退出了')
                    break
                logger.info('client:%s:%s, type:%s, len:%s, data: %s', addr[0], addr[1], type(data_bin), len(data_bin),
                            data_bin)
                try:
                    logger.info('client:%s:%s, type:%s, len:%s, data: %s', addr[0], addr[1], type(data_bin.hex()), len(data_bin.hex()), data_bin.hex())
                except Exception as ex:
                    logger.error(ex)
                try:
                    data_map = SL651Protocol().decode2(data_bin)
                except Exception as ex:
                    logger.error('decode error', exc_info=1)
                    continue
                if self.__callBack:
                    try:
                        self.__callBack.on_response(data_map)
                    except Exception as ex:
                        logger.error('callBack.on_response', exc_info=1)
                # client.send(data_bin)
            except Exception:
                logger.error("client error", exc_info=1)
                break

    def __loop_forever(self):
        MaxBytes = 1024 * 1024
        try:
            while True:
                data, addr = self.__server.recvfrom(MaxBytes)  # 等待客户端连接a
                threading.Thread(target=self.handle_request, args=(data, addr,), name=addr).start()
                logger.info('%s, %s', addr, "连接上了")

        except BaseException as e:
            logger.info("出现异常：%s", datetime.now(), exc_info=1)
        except KeyboardInterrupt as e:
            logger.warning('用户关闭进程', exc_info=1)