# coding=utf-8
"""清环设备协议解析-HJ212-2017"""
import socket
import logging
import threading
from datetime import datetime
from .HJ212_2017 import HJ212Protocol

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.getLogger('SL651_2014').setLevel(logging.INFO)
from .service import CallbackI


class HJ212CallbackI(CallbackI):
    """统一回调接口"""

    def on_response(self, data):
        """

        @type data: SL651Response
        @return:
        """
        raise NotImplemented()


class HJ212DeviceServer(object):
    """设备服务"""
    __callBack = None  # type: HJ212CallbackI
    __server = None  #

    def __init__(self, host, port, callbackClass):
        """

        @param callbackClass: CallbackI
        """
        self.__callBack = callbackClass
        address = (host, port)
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.settimeout(86400)
        while True:
            try:
                server.bind(address)  # 绑定端口
                break
            except Exception as ex:
                pass
        server.listen(10)  # 监听
        logger.info('connected successed')
        self.__server = server
        t = threading.Thread(target=self.__loop_forever)
        t.setDaemon(True)
        t.start()

    def handle_request(self, client, addr):
        """

        @type client: socket.socket
        @param addr:
        @return:
        """
        logger.debug((client, addr))
        MaxBytes = 1024 * 1024
        while True:
            try:
                data_bin = client.recv(MaxBytes)
                if not data_bin:
                    logger.info('数据为空，我要退出了')
                    break
                logger.info('client:%s:%s, type:%s, len:%s, data: %s', addr[0], addr[1], type(data_bin), len(data_bin),
                            data_bin)
                try:
                    data_str = data_bin.decode('UTF-8')
                    logger.info('client:%s:%s, type:%s, len:%s, data: %s', addr[0], addr[1], type(data_str),
                                len(data_str), data_str)
                except Exception as ex:
                    logger.error(ex)
                try:
                    data_map = HJ212Protocol().decode(data_bin)
                except Exception as ex:
                    logger.error('decode error', exc_info=1)
                    continue
                resp_msg = 'QN=20160801085857223;ST=91;CN=9011;PW=123456;MN=010000A8900016F000169DC0;Flag=4;CP=&&QnRtn=1&&'
                try:
                    client.send(resp_msg.encode('utf-8'))
                except Exception as ex:
                    logger.error('', exc_info=1)
                if self.__callBack:
                    try:
                        self.__callBack.on_response(data_map)
                    except Exception as ex:
                        logger.error('callBack.on_response', exc_info=1)
                # client.send(data_bin)
            except Exception:
                logger.error("client error", exc_info=1)
                break

    def __loop_forever(self):
        try:
            while True:
                client, addr = self.__server.accept()  # 等待客户端连接a
                threading.Thread(target=self.handle_request, args=(client, addr,)).start()
                logger.info('%s, %s', addr, "连接上了")

        except BaseException as e:
            logger.info("出现异常：%s", datetime.now(), exc_info=1)
        except KeyboardInterrupt as e:
            logger.warning('用户关闭进程', exc_info=1)