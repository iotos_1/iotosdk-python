# -*- coding:utf-8 -*-
# author : jiji time 12/10/2021
# Modbus tcp server
import sys
import modbus_tk.modbus_tcp as modbus_tcp
from modbus_tk.exceptions import ModbusInvalidResponseError
# reload(sys)
# sys.setdefaultencoding('utf8')

sys.path.append("..")
from driver import *

# 补码转为负数
def Complement2Negative(int_data):
    data = '0b'
    bin_data = bin(int_data).split('0b')[1]
    # print bin(int_data)
    if len(bin_data) < 16:
        return int_data
    else:
        for i in bin_data:
            if i == '1':
                data += '0'
            if i == '0':
                data += '1'
        return (int(data,2)+1)*-1

class Driver(IOTOSDriverI):
    #1、通信初始化
    def InitComm(self,attrs):

        self._HOST = self.sysAttrs['config']['param']['HOST']
        self._PORT = self.sysAttrs['config']['param']['PORT']
        self._master = modbus_tcp.TcpMaster(host=self._HOST, port=int(self._PORT))
        self._master.set_timeout(5.0)
        self.setPauseCollect(False)
        self.setCollectingOneCircle(False)
        self.online(True)

    # #2、采集引擎回调，可也可以开启，也可以直接注释掉（对于主动上报，不存在遍历采集的情况）
    def Collecting(self, dataId):
        try:
            cfgtmp = self.data2attrs[dataId]['config']
            # 过滤非modbus tcp配置的点
            if 'param' not in cfgtmp or 'proxy' not in cfgtmp:
                return ()

            # 当是新一组功能号时；当没有proxy.pointer，或者有，但是值为null时，就进行采集！否则（有pointer且值不为null，表明设置了采集代理，那么自己自然就被略过了，因为被代理了）当前数据点遍历轮询会被略过！
            if 'pointer' not in cfgtmp['proxy'] or cfgtmp['proxy']['pointer'] == None or cfgtmp['proxy']['pointer'] == '':
                # 某些过滤掉不采集，因为有的地址的设备不在线，只要在proxy下面配置disabled:true，这样就不会轮训到它！
                if 'disabled' in cfgtmp['proxy'] and cfgtmp['proxy']['disabled'] == True:
                    return ()
                else:
                    self.warn(self.name(dataId))

            # 过滤非modbus rtu配置的点
            if 'funid' not in cfgtmp['param']:
                return ()

            # 功能码
            funid = cfgtmp['param']['funid']
            # 设备地址
            devid = cfgtmp['param']['devid']
            # 寄存器地址
            regad = cfgtmp['param']['regad']
            # 格式
            format = cfgtmp['param']['format']
            # 长度
            quantity = re.findall(r"\d+\.?\d*", format)
            if len(quantity):
                quantity = int(quantity[0])
            else:
                quantity = 1
            if format.lower().find('i') != -1:  # I、i类型数据为4个字节，所以n个数据，就是4n字节，除一般应对modbus标准协议的2字节一个数据的个数单位！
                quantity *= 4 / 2
            elif format.lower().find('h') != -1:
                quantity *= 2 / 2
            elif format.lower().find('b') != -1:
                quantity *= 1 / 2
            elif format.find('d') != -1:
                quantity *= 8 / 2
            elif format.find('f') != -1:
                quantity *= 4 / 2
            elif format.find(
                    '?') != -1:  # 对于功能号1、2的开关量读，开关个数，对于这种bool开关型，个数就不是返回字节数的两倍了！返回的字节个数是动态的，要字节数对应的位数总和，能覆盖传入的个数数值！
                quantity *= 1
                format = ''  # 实践发现，对于bool开关型，传入开关量个数就行，format保留为空！如果format设置为 "?"或"8?"、">?"等，都会解析不正确！！
            self.debug(
                '>>>>>>' + '(PORT-' + str(self._PORT) + ')' + str(devid) + ' ' + str(funid) + ' ' + str(regad) + ' ' + str(
                    quantity) + ' ' + str(format))
            rtu_ret = self._master.execute(devid, funid, regad, quantity, data_format=format)
            self.debug(rtu_ret)

            # 私有modbus解析
            if 'private' in cfgtmp['param']:
                # 温湿度传感器
                if cfgtmp['param']['private'] == 'Temp&Hum':
                    data_list = []
                    for i in rtu_ret:
                        data_list.append(Complement2Negative(i)*0.1)
                    rtu_ret = tuple(data_list)

            return rtu_ret
        except ModbusInvalidResponseError as e:
            self.error(u'MODBUS响应超时, ' + e.message)
            return None
        except Exception as e:
            traceback.print_exc(e.message)
            self.error(u'采集解析参数错误：' + e.message)
            return None
        
        
        

    def Event_setData(self, dataId, value):
        self.warn(value)
        try:
            if self._master == None:
                self.InitComm()
            data_config = self.data2attrs[dataId]['config']
            cfgtmp = data_config
            # 230616，兼容按照配置来下发控制
            funid = cfgtmp['param']['funid']
            if not funid:
                funid = cst.WRITE_MULTIPLE_COILS;
            devid = cfgtmp['param']['devid']
            if not devid:
                devid = 1;
            regad = cfgtmp['param']['regad']
            if not regad:
                regad = 0;
            '''230616，在collecting中，以format字段是否有配置，来作为是读还是写数据点。暂未以数据点的读写属性为依据！写的时候默认只写当前寄存器一个，而不是能写入多个！
            也就是说写入的长度就是1因此也不需要format参数'''
            # format = cfgtmp['param']['format']
            bit = 0
            valtmp = self.valueTyped(dataId, value)  # 230616
            if 'proxy' in data_config.keys() and 'pointer' in data_config['proxy'] and data_config['proxy']['pointer'] != None:
                bit = data_config['proxy']['index']
            if valtmp == True or valtmp == 1:
                self.bitsState[bit] = 1
                valtmp = self.bitsState
            elif valtmp == False or valtmp == 0:
                self.bitsState[bit] = 0
            else:
                self.bitsState[bit] = valtmp
                self.warn(valtmp)

            #支持兼容功能号为批量写入开关控制或模拟量输入，但是都是对当前数据点的操作
            if funid == 15 or funid == 16:
                valtmp = self.bitsState

                # 注意，这里地址是1，但是再huaihua等用了3合一设备的，地址是2，接下来需要这里也做个区分，按照当前操作的数据点对应的实际数据点来！
                ret = self._master.execute(devid, funid, regad, output_value=valtmp)    #tips 240116，下发数值列表
                self.warn(ret)
                return json.dumps({'code': 0, 'msg': u'操作成功！', 'data': list(ret)})
            else:#240111，数据点配置的功能码不是15/16，而是5/6等下发控制时，走这里，需要进一步结合功能码和解析进一步测试
                ret = self._master.execute(devid, funid, regad, output_value=valtmp)    #tips 240116，下发单个数值
                self.warn(ret)
                return json.dumps({'code': 0, 'msg': u'操作成功！', 'data': list(ret)})

        except Exception as e:
            return json.dumps({'code': 501, 'msg': u'操作失败，错误码501，' + e.message, 'data': None})