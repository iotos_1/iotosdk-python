#!coding:utf8
from requests import Session
import requests
import time
import threading
from driver import *
import json
import sys

sys.path.append("..")
sys.setdefaultencoding( "utf-8" )
from driver import *

import requests
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings()

class Driver(IOTOSDriverI):
    # 1、通信初始化
    def InitComm(self, attrs):
        self.setPauseCollect(False)
        self.setCollectingOneCircle(False)
        self.online(True)

    # 2、采集
    def Collecting(self, dataId):
        param = {}
        devName = self.sysAttrs['name']
        headers = {
            'Content-Type': 'text/plain',
        }
        #目前用的是当前上报的时间，本应该用查询到的数据的时间！！临时先这样
        param[devName] = {
            "sensorId": self.name(dataId),
            "@column": "sensorVal,sensorDate",
            "@order": "sensorDate-"
        }
        param["format"] = True        
        self.warn(json.dumps(param))
        res = requests.post(url='http://sys.aiotos.net:9081/get?',headers = headers,data=json.dumps(param))
        val = json.loads(res.text)[devName.lower()]['sensorVal'];
        return [float(val)]; #当前返回数据要用[]括起来

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO

        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************

        TODO

        **************************************************'''
        data = None
        return json.dumps({'code': 0, 'msg': '', 'data': data})

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):



        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):

        return json.dumps({'code': 0, 'msg': '', 'data': ''})
