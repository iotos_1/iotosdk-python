#include "ZMiotDriverI.h"
#include "Python.h"

ZMIotDriverI::ZMIotDriverI(){

}


ZMIotDriverI::~ZMIotDriverI(){

}

std::string ZMIotDriverI::pointId(const std::string &dataId){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(s)", dataId);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["pointId"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

std::string ZMIotDriverI::name(const std::string &id){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(s)", id);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["name"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

std::string ZMIotDriverI::id(const std::string &name){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(s)", name);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["id"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

std::string ZMIotDriverI::online(bool state){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(i)", state);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["online"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}


std::string ZMIotDriverI::setValue(const std::string &dataId, const std::string &value){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(s,s)", dataId, value);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["setValue"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

std::string ZMIotDriverI::value(const std::string &dataId, const std::string &param, const std::string& source){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(s,s,s)", dataId, param, source);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["value"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

std::string ZMIotDriverI::subscribers(const std::string &dataId){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(s)", dataId);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["subscribers"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

///////////////////////////////////////////////////////////////////////////////////////////

string ZMIotDriverI::getSysId(){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("()");
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["getSysId"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

string ZMIotDriverI::getSysAttrs(){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("()");
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["getSysAttrs"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

string ZMIotDriverI::getData2attrs(){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("()");
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["getData2attrs"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

string ZMIotDriverI::getData2subs(){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("()");
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["getData2subs"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
		return "";
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	const char *nameString = PyString_AsString(pRetVal);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
	return nameString;
}

void ZMIotDriverI::setCollectingOneCircle(bool enable){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(i)",enable);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["setCollectingOneCircle"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
}

void ZMIotDriverI::setPauseCollect(bool enable){
	PyObject* pArgs = NULL;
	PyObject* pRetVal = NULL;
	pArgs = Py_BuildValue("(i)", enable);
	PyObject *funcPointer = (PyObject *)this->m_name2cbFunc["setPauseCollect"];
	if (!funcPointer){
		cout << "callback function pointId not found!" << endl;
	}
	pRetVal = PyEval_CallObject(funcPointer, pArgs);
	Py_DECREF(pArgs);
	Py_DECREF(pRetVal);
}