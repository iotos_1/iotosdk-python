#pragma once

#include <vector>
#include <map>
#include <string>
#include<iostream>
using namespace std;

class ZMIotDriverI
{
public:
	ZMIotDriverI();
	virtual ~ZMIotDriverI() = 0;

	//一、重写事件函数
	// #1、通信初始化
	virtual void InitComm(const string &params) = 0;
	// #2、采集
	virtual string Collecting(const string &dataId) = 0;
	// #3、控制
	// # A、广播事件
	virtual string Event_customBroadcast(const string &fromUuid, const string &type, const string &data) = 0;
	// # B、事件回调，数据点读操作
	virtual string Event_getData(const string &dataId, const string &condition) = 0;
	// # C、事件回调，数据点写操作
	virtual string Event_setData(const string &dataId, const string &value) = 0;
	// # D、事件回调，本地订阅数据的发布事件。账户下订阅的所有监测点进行pub时，会到所有接入点的所有设备驱动中走一遍，以数据点全id传入。
	virtual string Event_syncPubMsg(const string &point, const string &value) = 0;

	//二、成员方法
	string pointId(const string &dataId);
	string name(const string &id);
	string id(const string &name);
	string online(bool state = true);
	string setValue(const string &dataId, const string &value);
	string value(const string &dataId, const string &param, const std::string& source = "memory");
	string subscribers(const string &dataId);

	//三、属性读写
	string getSysId();
	string getSysAttrs();
	string getData2attrs();
	string getData2subs();
	void   setCollectingOneCircle(bool enable = true);
	void   setPauseCollect(bool enable = true);

protected:
	// 更新数据点的值 返回0表示更新成功，非0表示更新失败（-1：点不存在，1：不需要更新）
	//int refreshLocalData(const string dataId, const string &value);

public:
	map<string, void*> m_name2cbFunc;
};
