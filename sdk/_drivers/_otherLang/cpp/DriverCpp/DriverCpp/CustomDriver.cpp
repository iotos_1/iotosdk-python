
#include "CustomDriver.h"
CustomDriver::CustomDriver(){
	Json::Reader reader;
	this->m_sysId = this->getSysId();
	reader.parse(this->getSysAttrs(), this->m_sysAttrs);
	reader.parse(this->getData2attrs(), this->m_data2attrs);
	reader.parse(this->getData2subs(), this->m_data2subs);
}


CustomDriver::~CustomDriver(){

}

// #1、通信初始化
void CustomDriver::InitComm(const std::string &params){	
	//是否开启采集循环（Collecting）
	this->setPauseCollect(false);
	//是否只执行一遍循环
	this->setCollectingOneCircle(true);
	/**********************************************************
	
							TODO	

	***********************************************************/
	//设备上线
	this->online(true);
}

/* 
#2、采集（直接返回值的字符串形式，不需要code、msg、data 3个字段构成的json结构！）
数据点逐个自动遍历，根据配置属性，进行设备接口调用协议解析，数据返回
*/
std::string CustomDriver::Collecting(const std::string &dataId){
	/**********************************************************

	
							TODO

	***********************************************************/
	return "112233.44";
}

/*
#广播事件（较少用到）
广播事件处理，传入对方全ID，操作类型，以及请求参数
*/
std::string CustomDriver::Event_customBroadcast(const std::string &fromUuid, const std::string &type, const std::string &data){
	Json::Value jsonRet, jsonData;
	jsonRet["code"] = 0;
	jsonRet["msg"] = "OK";
	/**********************************************************


							TODO

	***********************************************************/
	jsonRet["data"].append(jsonData);
	return jsonRet.toStyledString();
}

// #查询事件
std::string CustomDriver::Event_getData(const std::string &dataId, const std::string &condition){
	Json::Value jsonRet, jsonData;
	jsonRet["code"] = 0;
	jsonRet["msg"] = "OK";
	/**********************************************************

							
							TODO

	***********************************************************/
	jsonRet["data"].append(jsonData);
	return jsonRet.toStyledString();
}

// #控制事件
std::string CustomDriver::Event_setData(const std::string &dataId, const std::string &value){
	Json::Value jsonRet, jsonData;
	jsonRet["code"] = 0;
	jsonRet["msg"] = "OK";
	/**********************************************************

							
							TODO

	***********************************************************/
	jsonRet["data"].append(jsonData);
	return jsonRet.toStyledString();
}

// #本地订阅发布事件
std::string CustomDriver::Event_syncPubMsg(const std::string &point, const std::string &value){
	Json::Value jsonRet, jsonData;
	jsonRet["code"] = 0;
	jsonRet["msg"] = "OK";
	/**********************************************************

	
							TODO

	***********************************************************/
	jsonRet["data"].append(jsonData);
	return jsonRet.toStyledString();
}