#include <Python.h>
#include "..\CustomDriver.h"
#include <map> 
using namespace std;

ZMIotDriverI* driverInst = NULL;

//--------------------------------测试函数-------------------------------

void console(const char *name)
{
	printf("%s is flying.\n", name);
}

static PyObject *consoleTest(PyObject *self, PyObject *args)
{
	const char *name;
	if (!PyArg_ParseTuple(args, "s", &name))
		return NULL;
	
	console(name);							//void返回的，

	return (PyObject*)Py_BuildValue("");	//这里以这种方式return
}

//------------------------回调函数由SDK实现、用户调用---------------------

static PyObject *gc_before_extract = NULL;

/// 设置回调
static PyObject *setCallback(PyObject *dummy, PyObject *args)
{
	const char *funcName = NULL;
	PyObject *callback = NULL;
	if (PyArg_ParseTuple(args, "sO", &funcName, &callback)) {				//获取参数元组，注意格式中，s为小写，对应解析第一个字符串变量，第二个要用大写欧O，小写还不行会报错！
		if (!PyCallable_Check(callback)) {
			PyErr_SetString(PyExc_TypeError, "parameter must be callable");
		}
		Py_XINCREF(callback);					/* Add a reference to new callback */
		//Py_XDECREF(gc_before_extract);		/* Dispose of previous callback */
		//gc_before_extract = callback;			/* Remember new callback */
		cout << ">" << funcName << " -- registed!" << endl;
		driverInst->m_name2cbFunc.insert(make_pair(funcName, callback));
	}
	return Py_BuildValue("l", (callback == NULL) ? 0 : 1);
}

//// 调用上面函数设置的python脚本函数
//int BeforeExt(char *pBeforeExtract)
//{
//	PyObject* pArgs = NULL;
//	PyObject* pRetVal = NULL;
//	int nRetVal = 0;
//	pArgs = Py_BuildValue("(s)", "my_file_name");
//	pRetVal = PyEval_CallObject(gc_before_extract, pArgs);
//	if (pRetVal)
//	{
//		fprintf(stderr, "PyEval_CallObject : ok \r\n");
//		nRetVal = PyInt_AsLong(pRetVal);
//		fprintf(stderr, "PyEval_CallObject : return : %d \r\n", nRetVal);
//	}
//	Py_DECREF(pArgs);
//	Py_DECREF(pRetVal);
//	return nRetVal;
//}

// 测试函数
static PyObject* testCallback(PyObject *self, PyObject *args)		//1、PyObject* 类型的传参，都是可以通过PyArg_ParseTuple元组方式来解析分离得到的
{
	Py_ssize_t sizetmp = PyTuple_Size(args);						//2、通过PyTuple_Size就可以得到传入参数对象的元组元素个数
	PyObject *cbName = PyTuple_GetItem(args, 0);					//3、通过PyTuple_GetItem可以获取指定索引的传参元组中指定参数
	Py_XINCREF(cbName);												//注：PyTuple_GetItem不会增加引用计数，所以要持有对象，必须Py_XINCREF，Py_Tuple_SetItem会对olditem进行Py_XDECREF。
	PyObject *pArgs = PyTuple_New(sizetmp - 1);
	for (int i = 1; i < sizetmp; i++) {
		PyTuple_SetItem(pArgs, i - 1, PyTuple_GetItem(args, i));	//4、通过PyTuple_SetItem可以重新组装PyObject*类型的数据重新组装成PyObject*元组数据
	}

	//PyObject *pArgs = PyTuple_New(2);
	//PyTuple_SetItem(pArgs, 0, Py_BuildValue("s", "welcome"));//0—序号 i表示创建int型变量
	//PyTuple_SetItem(pArgs, 1, Py_BuildValue("s", "iotos"));
	//result = PyEval_CallObject(gc_before_extract, pArgs);

	const char *nameString = PyString_AsString(cbName);
	PyObject * result = NULL;
	result = PyEval_CallObject((PyObject *)driverInst->m_name2cbFunc[nameString], pArgs);

	if (result)
	{
		Py_DECREF(cbName);
		//Py_DECREF(result);										//5、python的回调函数返回字符串时，这里不做Py_DECREF释放，会内存泄露；而这里释放后再return，会报错！
	}																
	return result;													//6、好像即时这里加了，同时不返回这个字符，python端也会报内存泄露警告
}

//string invokeCallback(string funcName,)

//------------------------派生函数由SDK调用、用户实现-----------------------

static PyObject *InitComm(PyObject *self, PyObject *args)
{
	const char *name;
	if (!PyArg_ParseTuple(args, "s", &name))
		return NULL;

	driverInst->InitComm(name);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *Collecting(PyObject *self, PyObject *args)
{
	const char *param;
	if (!PyArg_ParseTuple(args, "s", &param))
		return NULL;

	return (PyObject*)Py_BuildValue("s", driverInst->Collecting(param));
}

static PyObject *Event_customBroadcast(PyObject *self, PyObject *args)
{
	const char *fromUuid, *type, *data;
	if (!PyArg_ParseTuple(args, "sss", &fromUuid, &type, &data))
		return NULL;

	return (PyObject*)Py_BuildValue("s", driverInst->Event_customBroadcast(fromUuid, type, data).c_str());
}

static PyObject *Event_getData(PyObject *self, PyObject *args)
{
	const char *dataId, *condition;
	if (!PyArg_ParseTuple(args, "ss", &dataId, &condition))
		return NULL;

	return (PyObject*)Py_BuildValue("s", driverInst->Event_getData(dataId, condition).c_str());
}

static PyObject *Event_setData(PyObject *self, PyObject *args)
{
	const char *dataId, *value;
	if (!PyArg_ParseTuple(args, "ss", &dataId, &value))
		return NULL;

	return (PyObject*)Py_BuildValue("s", driverInst->Event_setData(dataId, value).c_str());
}

static PyObject *Event_syncPubMsg(PyObject *self, PyObject *args)
{
	const char *point, *value;
	if (!PyArg_ParseTuple(args, "ss", &point, &value))
		return NULL;

	return (PyObject*)Py_BuildValue("s", driverInst->Event_syncPubMsg(point, value).c_str());
}

//------------------------函数定义和注册-----------------------

static PyMethodDef consoleTest_methods[] = {
		{ "console", consoleTest, METH_VARARGS, "no doc info" },
		{ "setCallback", setCallback, METH_VARARGS, "no doc info" },
		{ "testCallback", testCallback, METH_VARARGS, "no doc info" },
		{ "InitComm", InitComm, METH_VARARGS, "no doc info" },
		{ "Collecting", Collecting, METH_VARARGS, "no doc info" },
		{ "Event_customBroadcast", Event_customBroadcast, METH_VARARGS, "no doc info" },
		{ "Event_getData", Event_getData, METH_VARARGS, "no doc info" },
		{ "Event_setData", Event_setData, METH_VARARGS, "no doc info" }, 
		{ "Event_syncPubMsg", Event_syncPubMsg, METH_VARARGS, "no doc info" },
		{ NULL, NULL, 0, NULL }
};

//-------------------------PY模块初始化--------------------------

PyMODINIT_FUNC initDriverCpp(void)
{
	PyImport_AddModule("DriverCpp");
	Py_InitModule("DriverCpp", consoleTest_methods);
	driverInst = new CustomDriver;
}