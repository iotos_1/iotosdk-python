#!coding:utf8
import time
import threading
from driver import *
import json
import sys

sys.path.append("..")
from driver import *
from dr_influxdb import InfluxDB

class Driver(IOTOSDriverI):
    # 1、通信初始化
    def InitComm(self, attrs):
        self.influx = InfluxDB().influx
        self.setPauseCollect(True)
        self.setCollectingOneCircle(True)
        self.online(True)

    # 2、采集
    def Collecting(self, dataId):
        
        return ()

    # 3、控制
    # 事件回调接口，其他操作访问
    def Event_customBroadcast(self, fromUuid, type, data):
        '''*************************************************

        TODO

        **************************************************'''
        return json.dumps({'code': 0, 'msg': '', 'data': ''})

    # 3、查询
    # 事件回调接口，监测点操作访问
    def Event_getData(self, dataId, condition):
        '''*************************************************

        TODO

        **************************************************'''
        data = None
        return json.dumps({'code': 0, 'msg': '', 'data': data})

    def timerProcess(self,name,value):
        self.setValue(name, value, auto_created = True);
        # self.setValue(methodtmp, res['value'], auto_created = True);

    # 事件回调接口，监测点操作访问
    def Event_setData(self, dataId, value):
        '''传入value格式如：method为数据库sql聚合函数名称，支持min/max/sum/count等等
        {
            "device_oid":"91dc45a9",
            "data_oid":"a939",
            "start_time":"2023-07-03 16:13:56",
            "end_time":"2023-07-23 16:13:56",
            "method":"min"
        }
        '''
        result = None
        res = {}
        paramtmp = None
        methodtmp = None
        try:
            paramtmp = json.loads(value)
            methodtmp = paramtmp['method']
            result = self.influx.query(
                "select " + methodtmp + "(value) from \
                    iot_point_float where \
                        dataoid='{}' and \
                        devoid='{}' and \
                        time >= '{}' and \
                        time < '{}' and \
                        (is_parent='' or is_parent='1') \
                        tz('Asia/Shanghai')".format(
                paramtmp['data_oid'], 
                paramtmp['device_oid'],
                paramtmp['start_time'],
                paramtmp['end_time']
            ), epoch='ms')                                
        except Exception as ex:
            self.error(value)
            self.error(ex)
        if result:
            for info in result.items()[0][1]:
                self.error(info)
                res["value"] = info[methodtmp]
                res["time"] =  info["time"]
        self.error(res['value'])
        #需要通过定时器异步去setValue上报，否则当前函数的return返回始终会覆盖setValue上报的值！
        threading.Timer(0.1, self.timerProcess,(self.name(dataId), res['value'])).start()
        return json.dumps({'code': 0, 'msg': '', 'data': res})

    # 事件回调接口，监测点操作访问
    def Event_syncPubMsg(self, point, value):

        return json.dumps({'code': 0, 'msg': '', 'data': ''})
