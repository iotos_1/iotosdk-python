#coding=utf-8
import sys
sys.path.append("..")
from driver import *
import requests
import json
import time
import ctypes
reload(sys)
sys.setdefaultencoding('utf8')


baseUrl = "http://183.247.193.91:9999/air-cloud/a/air/api/"

def DeviceInfo(self):
    url = baseUrl + "apiDeviceInfo?model=" + self.airModel + "&mobile=" + self.airMobile + "&password=" + self.airPassword
    r = requests.get(url)
    dicts = json.loads(r.text)
    for dic in dicts["data"]:
        if str(dic["id"]) is not self.airID.encode("utf-8"):
            continue
        else:
            self.conoff=dic["conoff"]
            self.cmode=dic["cmode"]
            self.ctemp=dic["ctemp"]
            self.cwind=dic["cwind"]
            self.cwinddir=dic["cwinddir"]
            self.debug("111111111111123456789098763"+self.conoff)

            #上传数据
            self.setValue(u'设备名称', dic["name"])
            self.setValue(u'设备类型', dic["model"])
            self.setValue(u'网关名称', dic["gateway_name"])
            self.setValue(u'产品ID', dic["product_id"])
            self.setValue(u'产品名称', dic["product_name"])
            self.setValue(u'状态', dic["status"])
            self.setValue(u'创建时间', dic["create_date"])
            self.setValue(u'修改时间', dic["update_date"])
            self.setValue(u'修改者', dic["update_user_name"])
            self.setValue(u'温度', dic["ctemp"])
            self.setValue(u'开关', dic["conoff"])
            self.setValue(u'模式', dic["cmode"])
            self.setValue(u'风速', dic["cwind"])
            self.setValue(u'风向', dic["cwinddir"])
            self.setValue(u'用电量', dic["usedele"])
            self.setValue(u'用电时长', dic["usedtime"])
            self.setValue(u'有人无人',dic["person"])
            self.setValue(u'湿度',dic["humidity"])
            self.setValue(u'光照度',dic["photositive"])
            self.setValue(u'室内温度',dic["intemp"])

#控制空调
def infrared(self):
    url = baseUrl + "infrared?airid=" + self.airID + "&conoff=" + self.airOnoff + "&cmode=" + self.airMode + "&ctemp=" + self.airTemp + "&cwind=" + self.airWind + "&cwinddir=" + self.airWinddir
    r = requests.get(url)
    if r.status_code == 200:
        return json.dumps({'code':200, 'msg':'修改成功！', 'data':''})
    else:
        return json.dumps({'code':'=.=', 'msg': '修改失败！', 'data': ''})

#用来更新用于下发的数据点的值
def fk(self):
    conoff={
        "开":"0",
        "关":"1"
    }
    cmode={
        "自动": "0",
        "制冷": "1",
        "除湿": "2",
        "送风": "3",
        "制热": "4"
    }
    cwind={
        "自动": "0",
        "低风": "1",
        "中风": "2",
        "高风": "3",
    }
    cwinddir = {
        "自动": "0",
        "风向1": "1",
        "风向2": "2",
        "风向3": "3",
    }
    fkdata=conoff[self.conoff.encode('utf-8')]+','+cmode[self.cmode.encode('utf-8')]+','+str(self.ctemp)+','+cwind[self.cwind.encode('utf-8')]+','+cwinddir[self.cwinddir.encode('utf-8')]
    # self.debug(fkdata)
    self.setValue(u'反控点',fkdata)  #给反控点赋值

class Project(IOTOSDriverI):
    # 数据初始化
    def __init__(self):
        IOTOSDriverI.__init__(self)

    # 通讯初始化
    def InitComm(self, attrs=None):
        self.online(True)
        self.setPauseCollect(False)
        self.setCollectingOneCircle(False)
        try:
            #获取设备类型、设备标识等配置属性
            self.airModel = self.sysAttrs['config']['param']['model']
            self.airID = self.sysAttrs['config']['param']['id']
            self.airMobile = self.sysAttrs['config']['param']['mobile']
            self.airPassword = self.sysAttrs['config']['param']['password']
            self.debug(ctypes.create_string_buffer(bytes(self.airModel.encode('ascii'))))
            self.debug(self.airModel)
            self.debug(type(self.airModel.encode()))
            self.debug(self.sysAttrs['name'] + u' 参数已获取！')
        except Exception, e:
            self.debug(u'设备参数配置获取失败' + e.message)

    def Collecting(self,dataId):
        try:
            self.debug(dataId)
            DeviceInfo(self) #上传数据
            fk(self) #更新反控点的值

        except Exception, e:
            self.debug(u'数据上传失败' + e.message)
        time.sleep(7)
        return ()

    def Event_setData(self, dataId, value):
        try:
            # if dataId == "aa50":
            data_attrs = self.data2attrs[dataId]['config']
            # 判断参数里是否有private属性
            if data_attrs['param'].has_key('private'):
                # 确定private属性的值是否为空调对应的airCloud
                if data_attrs['param']['private'] == 'aircloud':
                    self.debug("yep!")
                    self.airData = value
                    fk=self.airData.split(',')
                    self.debug(fk)
                    #获取下发数值并处理
                    self.airOnoff = fk[0]
                    self.airMode = fk[1]
                    self.airTemp = fk[2]
                    self.airWind = fk[3]
                    self.airWinddir = fk[4]
                    infrared(self)  #控制
                    DeviceInfo(self)  # 更新
        except Exception, e:
            self.debug(u'修改失败！！！' + e.message)
        return json.dumps({'code': 0, 'msg': '', 'data': ''})